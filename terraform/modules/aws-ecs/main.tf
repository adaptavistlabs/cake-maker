module "labels" {
  source    = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=tags/0.5.1"
  namespace = var.namespace
  stage     = var.stage
  name      = var.name
  tags      = var.tags
}

resource "aws_ecs_cluster" "this" {
  tags = module.labels.tags
  name               = module.labels.id
  capacity_providers = ["FARGATE"]
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

data "template_file" "taskdef" {
  template = file("${path.module}/taskdef.tpl")
  vars = {
    region         = var.region
    log_group_name = aws_cloudwatch_log_group.this.name
  }
}

resource "aws_ecs_task_definition" "this" {
  tags = module.labels.tags
  container_definitions = data.template_file.taskdef.rendered
  family = module.labels.name
  requires_compatibilities = ["FARGATE"]
  memory = 8192
  cpu = 4096
  network_mode = "awsvpc"
  execution_role_arn = aws_iam_role.ecs_task_execution.arn
}

resource "aws_ecs_service" "this" {
  name = module.labels.id
  tags = module.labels.tags
  cluster = aws_ecs_cluster.this.id
  task_definition = aws_ecs_task_definition.this.arn
  desired_count = 1
  launch_type = "FARGATE"
  platform_version = "1.4.0"
  health_check_grace_period_seconds = 300
}

resource "aws_iam_role" "ecs_task_execution" {
  name_prefix        = module.labels.id
  assume_role_policy = data.aws_iam_policy_document.ecs_assume.json
  tags               = module.labels.tags
}

data "aws_iam_policy_document" "ecs_assume" {
  statement {
    actions = [
      "sts:AssumeRole"]

    principals {
      identifiers = [
        "ecs-tasks.amazonaws.com"]
      type = "Service"
    }
  }
}

resource "aws_iam_policy_attachment" "ecs_task_execution" {
  name       = aws_iam_role.ecs_task_execution.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  roles      = [aws_iam_role.ecs_task_execution.name]
}

resource "aws_cloudwatch_log_group" "this" {
  name = "/fargate/service/cake-maker"
  tags = module.labels.tags
}