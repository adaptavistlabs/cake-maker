variable "tags" {
  type        = map(string)
  default     = {}
  description = "A map of properties representing tags for each resource"
}

variable "name" {
  type = string
}

variable "namespace" {
  type = string
}

variable "stage" {
  type = string
}

variable "region" {
  type = string
}