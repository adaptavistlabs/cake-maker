[
 {
  "name": "cake-maker",
  "image": "",
  "logConfiguration": {
   "logDriver": "awslogs",
   "options": {
    "awslogs-group": "${log_group_name}",
    "awslogs-region": "${region}",
    "awslogs-stram-prefix": "ecs"
   }
  }
 }
]