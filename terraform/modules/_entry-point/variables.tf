variable "region" {
  type        = string
  description = "AWS region this infrastructure stack will be deployed to"
}

variable "assume_role_arn" {
  type = string
  description = "ARN of the role that will be assumed by terraform"
}

variable stage {
  type        = string
  description = "Deployment stage i.e. environment name"
}

variable stage_type {
  type        = string
  description = "The type of workloads this stage is running"
}