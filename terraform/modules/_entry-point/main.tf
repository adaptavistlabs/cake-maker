locals {
  namespace = "ml"
  name = "cake-maker"
  tags = {
    "Avst:BusinessUnit" = "analytics"
    "Avst:Product" = "machine-learning"
    "Avst:Project" = "cake-maker"
    "Avst:Stage:Name" = var.stage
    "Avst:Stage:Type" = var.stage_type
    "Avst:CostCenter" = ""
    "Avst:Src" = ""
  }
}

provider "aws" {
  region = var.region

  assume_role {
    role_arn = var.assume_role_arn
  }
}

module "labels" {
  source    = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=tags/0.5.1"
  namespace = local.namespace
  stage     = var.stage
  name      = local.name
  tags      = local.tags
}

module "s3" {
  source = "Adaptavist/aws-s3-encrypted-private/module"
  version = "1.1.1"

  namespace = local.namespace
  stage = var.stage
  name = "snapshots"
  tags = module.labels.tags


}

module "lambda" {
  source = "Adaptavist/aws-lambda/module"
  version = "1.9.0"

  namespace = local.namespace
  stage = var.stage
  name = "snapshots"
  tags = module.labels.tags
}

module "fargate" {
  source = "../aws-ecs"

  region = var.region
  namespace = local.namespace
  stage = var.stage
  name = "snapshots"
  tags = module.labels.tags
}

module "athena" {
  source  = "Adaptavist/aws-athena/module"
  version = "1.3.0"

  namespace = local.namespace
  stage     = var.stage
  name      = local.name
  tags      = module.labels.tags

  create_database = true

  queries = {}
}